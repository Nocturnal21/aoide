#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Aoide");

    ui->label_currentNote->setText("");

    connect(ui->pushButton_play, SIGNAL(toggled(bool)), this, SLOT(playToggled(bool)));

    current_bpm = 60;
    ui->spinBox_bpm->setValue(current_bpm);
    connect(ui->spinBox_bpm, SIGNAL(valueChanged(int)), this, SLOT(bpmChanged(int)));
    current_bpn = 4;
    ui->spinBox_bpn->setValue(current_bpn);
    connect(ui->spinBox_bpn, SIGNAL(valueChanged(int)), this, SLOT(bpnChanged(int)));

    ui->pushButton_a->setObjectName("A");
    ui->pushButton_as->setObjectName("AS");
    ui->pushButton_be->setObjectName("BE");
    ui->pushButton_c->setObjectName("C");
    ui->pushButton_cis->setObjectName("CIS");
    ui->pushButton_d->setObjectName("D");
    ui->pushButton_e->setObjectName("E");
    ui->pushButton_es->setObjectName("ES");
    ui->pushButton_f->setObjectName("F");
    ui->pushButton_fis->setObjectName("FIS");
    ui->pushButton_g->setObjectName("G");
    ui->pushButton_h->setObjectName("H");

    ui->verticalSlider_masterV->setObjectName("masterVolume");
    //////// disabled masterV
    ui->verticalSlider_masterV->setDisabled(true);

    ui->verticalSlider_metronomeV->setObjectName("metronomeVolume");
    ui->verticalSlider_soundsV->setObjectName("soundVolume");
    connect(ui->verticalSlider_masterV, SIGNAL(valueChanged(int)), this, SLOT(volumeChanged(int)));
    connect(ui->verticalSlider_metronomeV, SIGNAL(valueChanged(int)), this, SLOT(volumeChanged(int)));
    connect(ui->verticalSlider_soundsV, SIGNAL(valueChanged(int)), this, SLOT(volumeChanged(int)));

    connect(ui->pushButton_a,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_as,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_be,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_c,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_cis,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_d,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_e,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_es,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_f,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_fis,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_g,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));
    connect(ui->pushButton_h,SIGNAL(toggled(bool)), this, SLOT(noteToggled(bool)));

    /* MENU ACTIONS SETUP */
    set_metronome_clave = new QAction("Clave", this);
    set_metronome_clave->setCheckable(true);
    connect(set_metronome_clave, SIGNAL(triggered(bool)), this, SLOT(menuSetMClave(bool)));
    set_metronome_hihat_closed = new QAction("Hihat Closed", this);
    set_metronome_hihat_closed->setCheckable(true);
    connect(set_metronome_hihat_closed, SIGNAL(triggered(bool)), this, SLOT(menuSetMHihatClosed(bool)));
    set_metronome_rim = new QAction("Rim", this);
    set_metronome_rim->setCheckable(true);
    connect(set_metronome_rim, SIGNAL(triggered(bool)), this, SLOT(menuSetMRim(bool)));

    set_metronome_clave_emph = new QAction("Clave", this);
    set_metronome_clave_emph->setCheckable(true);
    connect(set_metronome_clave_emph, SIGNAL(triggered(bool)), this, SLOT(menuSetMClaveEmph(bool)));
    set_metronome_hihat_closed_emph = new QAction("Hihat Closed", this);
    set_metronome_hihat_closed_emph->setCheckable(true);
    connect(set_metronome_hihat_closed_emph, SIGNAL(triggered(bool)), this, SLOT(menuSetMHihatClosedEmph(bool)));
    set_metronome_rim_emph = new QAction("Rim", this);
    set_metronome_rim_emph->setCheckable(true);
    connect(set_metronome_rim_emph, SIGNAL(triggered(bool)), this, SLOT(menuSetMRimEmph(bool)));


    /* MENU SETUP */
    set_metronome_sound = ui->menuSet_Sound->addMenu("Set metronome sound");
    set_metronome_sound->addAction(set_metronome_clave);
    set_metronome_sound->addAction(set_metronome_hihat_closed);
    set_metronome_sound->addAction(set_metronome_rim);
    set_metronome_hihat_closed->setChecked(true);

    set_metronome_emph_sound = ui->menuSet_Sound->addMenu("Set emphasized metronome sound");
    set_metronome_emph_sound->addAction(set_metronome_clave_emph);
    set_metronome_emph_sound->addAction(set_metronome_hihat_closed_emph);
    set_metronome_emph_sound->addAction(set_metronome_rim_emph);
    set_metronome_clave_emph->setChecked(true);

    metronome_sound = "metronome-hihat-closed";
    metronome_sound_emph = "metronome-clave";

    set_soundset = ui->menuSet_Sound->addMenu("Select soundset");

    std::cout << "Main window loaded!" << std::endl;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::start() {
    for(std::vector<int>::iterator it = checked_notes.begin();
        it != checked_notes.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;

    if(checked_notes.size() > 0) {
        thread = new QThread;
        timer = new QTimer;
        soundWorker = new SoundThread(timer, &checked_notes, current_bpm, current_bpn);
        soundWorker->setMetronomeSound(metronome_sound,false);
        soundWorker->setMetronomeSound(metronome_sound_emph,true);
        connect(this, SIGNAL(finishSoundThread()), soundWorker, SLOT(stop()));
        connect(this, SIGNAL(finishSoundThread()), timer, SLOT(stop()));
        connect(this, SIGNAL(finishSoundThread()), thread, SLOT(quit()));
        connect(thread, SIGNAL(finished()), this, SLOT(threadFinished()));
        connect(timer, SIGNAL(timeout()), soundWorker, SLOT(process()));
        connect(this, SIGNAL(bpmNewValue(int)), soundWorker, SLOT(bpmChanged(int)));
        connect(this, SIGNAL(volumeChangedMaster(int)), soundWorker, SLOT(volumeChangedMaster(int)));
        connect(this, SIGNAL(volumeChangedMetronome(int)), soundWorker, SLOT(volumeChangedMetronome(int)));
        connect(this, SIGNAL(volumeChangedSound(int)), soundWorker, SLOT(volumeChangedSound(int)));
        connect(soundWorker, SIGNAL(nextNote(int)), this, SLOT(showNextNote(int)));

        timer->moveToThread(thread);
        soundWorker->moveToThread(thread);

        thread->start();
    } else {
        ui->pushButton_play->setChecked(false);
    }
}

void MainWindow::playToggled(bool checked)
{
    if(checked) {
        start();
    } else {
        emit finishSoundThread();
    }
}

void MainWindow::bpmChanged(int value) {
    current_bpm = value;
    emit bpmNewValue(current_bpm);
}

void MainWindow::bpnChanged(int value) {
    current_bpn = value;
}

void MainWindow::volumeChanged(int value) {
    QString sender_name = sender()->objectName();

    if(sender_name == "masterVolume") {
        emit volumeChangedMaster(value);
    } else if(sender_name == "metronomeVolume") {
        emit volumeChangedMetronome(value);
    } else if(sender_name == "soundVolume") {
        emit volumeChangedSound(value);
    }
}

void MainWindow::showNextNote(int note) {
    switch (note) {
    case A:
        ui->label_currentNote->setText("A");
        break;
    case AS:
        ui->label_currentNote->setText("AS");
        break;
    case BE:
        ui->label_currentNote->setText("B");
        break;
    case C:
        ui->label_currentNote->setText("C");
        break;
    case CIS:
        ui->label_currentNote->setText("CIS");
        break;
    case D:
        ui->label_currentNote->setText("D");
        break;
    case E:
        ui->label_currentNote->setText("E");
        break;
    case ES:
        ui->label_currentNote->setText("ES");
        break;
    case F:
        ui->label_currentNote->setText("F");
        break;
    case FIS:
        ui->label_currentNote->setText("FIS");
        break;
    case G:
        ui->label_currentNote->setText("G");
        break;
    case H:
        ui->label_currentNote->setText("H");
        break;
    default:
        break;
    }
}

void MainWindow::noteToggled(bool checked) {
    QString sender_name = sender()->objectName();

    if(sender_name == "A") {
        if(checked){
            std::cout << "A checked!" << std::endl;
            checked_notes.push_back(A);
        } else {
            std::cout << "A released!" << std::endl;
            removeCheckedNote(A);
        }

    } else if(sender_name == "AS") {
        if(checked){
            std::cout << "AS checked!" << std::endl;
            checked_notes.push_back(AS);
        } else {
            std::cout << "AS released!" << std::endl;
            removeCheckedNote(AS);
        }

    } else if(sender_name == "BE") {
        if(checked){
            std::cout << "BE checked!" << std::endl;
            checked_notes.push_back(BE);
        } else {
            std::cout << "BE released!" << std::endl;
            removeCheckedNote(BE);
        }
    } else if(sender_name == "C") {
        if(checked){
            std::cout << "C checked!" << std::endl;
            checked_notes.push_back(C);
        } else {
            std::cout << "C released!" << std::endl;
            removeCheckedNote(C);
        }

    } else if(sender_name == "CIS") {
        if(checked){
            std::cout << "CIS checked!" << std::endl;
            checked_notes.push_back(CIS);
        } else {
            std::cout << "CIS released!" << std::endl;
            removeCheckedNote(CIS);
        }

    } else if(sender_name == "D") {
        if(checked){
            std::cout << "D checked!" << std::endl;
            checked_notes.push_back(D);
        } else {
            std::cout << "D released!" << std::endl;
            removeCheckedNote(D);
        }

    } else if(sender_name == "E") {
        if(checked){
            std::cout << "E checked!" << std::endl;
            checked_notes.push_back(E);
        } else {
            std::cout << "E released!" << std::endl;
            removeCheckedNote(E);
        }

    } else if(sender_name == "ES") {
        if(checked){
            std::cout << "ES checked!" << std::endl;
            checked_notes.push_back(ES);
        } else {
            std::cout << "ES released!" << std::endl;
            removeCheckedNote(ES);
        }

    } else if(sender_name == "F") {
        if(checked){
            std::cout << "F checked!" << std::endl;
            checked_notes.push_back(F);
        } else {
            std::cout << "F released!" << std::endl;
            removeCheckedNote(F);
        }

    } else if(sender_name == "FIS") {
        if(checked){
            std::cout << "FIS checked!" << std::endl;
            checked_notes.push_back(FIS);
        } else {
            std::cout << "FIS released!" << std::endl;
            removeCheckedNote(FIS);
        }

    } else if(sender_name == "G") {
        if(checked){
            std::cout << "G checked!" << std::endl;
            checked_notes.push_back(G);
        } else {
            std::cout << "G released!" << std::endl;
            removeCheckedNote(G);
        }
    } else if(sender_name == "H") {
        if(checked){
            std::cout << "H checked!" << std::endl;
            checked_notes.push_back(H);
        } else {
            std::cout << "H released!" << std::endl;
            removeCheckedNote(H);
        }
    }
}

void MainWindow::removeCheckedNote(int note) {
    std::cout << "Old size: " << checked_notes.size() << std::endl;
    for(std::vector<int>::iterator it = checked_notes.begin(); it != checked_notes.end(); ++it) {
        if(*it == note) {
            checked_notes.erase(it);
            std::cout << "Deleted note " << note << std::endl;
            return;
        }
    }
}

void MainWindow::errorString(QString message) {
    std::cerr << message.toStdString() << std::endl;
}

void MainWindow::threadFinished() {
    std::cout << "Sound Thread finished!" << std::endl;
}

void MainWindow::menuSetMClave(bool triggered) {
    if(triggered) {
        set_metronome_hihat_closed->setChecked(false);
        set_metronome_rim->setChecked(false);
        metronome_sound = "metronome-clave";

        //soundWorker->setMetronomeSound("metronome-clave",false);
    }
}

void MainWindow::menuSetMHihatClosed(bool triggered) {
    if(triggered) {
        set_metronome_clave->setChecked(false);
        set_metronome_rim->setChecked(false);
        metronome_sound = "metronome-hihat-closed";

        //soundWorker->setMetronomeSound("metronome-hihat-closed",false);
    }
}

void MainWindow::menuSetMRim(bool triggered) {
    if(triggered) {
        set_metronome_clave->setChecked(false);
        set_metronome_hihat_closed->setChecked(false);
        metronome_sound = "metronome-rim";
        //soundWorker->setMetronomeSound("metronome-rim",false);
    }
}

void MainWindow::menuSetMClaveEmph(bool triggered) {
    if(triggered) {
        set_metronome_hihat_closed_emph->setChecked(false);
        set_metronome_rim_emph->setChecked(false);
        metronome_sound_emph = "metronome-clave";

        //soundWorker->setMetronomeSound("metronome-clave",true);
    }
}

void MainWindow::menuSetMHihatClosedEmph(bool triggered) {
    if(triggered) {
        set_metronome_clave_emph->setChecked(false);
        set_metronome_rim_emph->setChecked(false);
        metronome_sound_emph = "metronome-hihat-closed";
        //soundWorker->setMetronomeSound("metronome-hihat-closed",true);
    }
}

void MainWindow::menuSetMRimEmph(bool triggered) {
    if(triggered) {
        set_metronome_clave_emph->setChecked(false);
        set_metronome_hihat_closed_emph->setChecked(false);
        metronome_sound_emph = "metronome-rim";
        //soundWorker->setMetronomeSound("metronome-rim",true);
    }
}
