#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QMainWindow>
#include <QMediaPlayer>
#include <QSoundEffect>
#include <QFileInfo>
#include <QMap>
#include <QList>
#include <QThread>
#include <QTimer>
#include <iostream>
#include <cstdio>
#include <vector>
#include "soundthread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    enum enum_notes { A = 0, AS, BE, C, CIS, D, E, ES, F, FIS, G, H };
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void playToggled(bool checked);
    void noteToggled(bool checked);
    void threadFinished();
    void errorString(QString message);
    void showNextNote(int note);

private slots:
    void bpmChanged(int value);
    void bpnChanged(int value);
    void volumeChanged(int value);
    void menuSetMClave(bool triggered);
    void menuSetMHihatClosed(bool triggered);
    void menuSetMRim(bool triggered);
    void menuSetMClaveEmph(bool triggered);
    void menuSetMHihatClosedEmph(bool triggered);
    void menuSetMRimEmph(bool triggered);

signals:
    void finishSoundThread();
    void bpmNewValue(int value);
    void volumeChangedMaster(int value);
    void volumeChangedMetronome(int value);
    void volumeChangedSound(int value);

private:
    void start();
    void removeCheckedNote(int note);

    Ui::MainWindow *ui;
    QMediaPlayer *player;

    QThread* thread;
    QTimer* timer;
    SoundThread* soundWorker;

    std::vector<int> checked_notes;
    int current_bpm;
    int current_bpn;

    QAction* set_metronome_clave;
    QAction* set_metronome_hihat_closed;
    QAction* set_metronome_rim;
    QAction* set_metronome_clave_emph;
    QAction* set_metronome_hihat_closed_emph;
    QAction* set_metronome_rim_emph;
    QMenu* set_metronome_sound;
    QMenu* set_metronome_emph_sound;
    QMenu* set_soundset;

    QString metronome_sound;
    QString metronome_sound_emph;
};

#endif // MAINWINDOW_H
