#ifndef SOUNDTHREAD_H
#define SOUNDTHREAD_H

#include <QObject>
#include <QList>
#include <QMap>
#include <QUrl>
#include <QFileInfo>
#include <QSoundEffect>
#include <QTimer>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <vector>

class SoundThread : public QObject
{
    Q_OBJECT
public:
    enum enum_notes { A = 0, AS, BE, C, CIS, D, E, ES, F, FIS, G, H };
    SoundThread(QTimer* timer, const std::vector<int> *checked_notes, int bpm, int bpn);
    ~SoundThread();
    void setMetronomeSound(const QString& name, bool emphasized);

public slots:
    void bpmChanged(int value);
    void volumeChangedMaster(int value);
    void volumeChangedMetronome(int value);
    void volumeChangedSound(int value);
    void stop();

private slots:
    void process();

signals:
    void finished();
    void error(QString err);
    void nextNote(int note);

private:
    void loadFiles();
    void permutate();
    void nextNote();

    QMap<QString,QUrl> metronome_map;
    QSoundEffect* metronome;
    QSoundEffect* metronome_emph;

    QMap<int, QSoundEffect*> sound_map;
    const std::vector<int>* checked_notes;
    std::vector<int> permutation;
    std::vector<int>::iterator permutation_iterator;
    QSoundEffect* current_sound;

    int interval;
    int bpn;
    QTimer* timer;
    int beatsPerNote;
    int beatCycle;
    int countBeats;
    int current_note;
};

#endif // SOUNDTHREAD_H
