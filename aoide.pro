#-------------------------------------------------
#
# Project created by QtCreator 2016-02-24T14:30:45
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = aoide
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    soundthread.cpp

HEADERS  += mainwindow.h \
    soundthread.h

FORMS    += mainwindow.ui
