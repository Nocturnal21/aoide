#include "soundthread.h"

SoundThread::SoundThread(QTimer* timer, const std::vector<int> *checked_notes, int bpm, int bpn)
{
    this->timer = timer;
    bpmChanged(bpm);
    this->bpn = bpn;
    this->checked_notes = checked_notes;

    loadFiles();
    volumeChangedMetronome(100);
    volumeChangedSound(30);

    std::cout << "Sounds loaded: " << sound_map.size() << std::endl;

    this->timer->start(interval);
    this->beatCycle = bpn;
    this->beatsPerNote = bpn;
    this->countBeats = 0;

    permutate();
}

SoundThread::~SoundThread()
{

}

void SoundThread::process() {
    /* METRONOME */
    if(countBeats % beatCycle == beatCycle - 1) {    //emphasize last beat
        metronome_emph->play();
        current_note = (int)checked_notes->at(*permutation_iterator);
        std::cout << "Next note: " << current_note << std::endl;
        emit nextNote(current_note);
    } else
        metronome->play();

    /* SOUND */
    if(countBeats >= beatsPerNote) {
        if(countBeats % beatsPerNote == 0) {
            if(countBeats > beatsPerNote) {
                current_sound->stop();
            }
            current_sound = sound_map[current_note];

            /* PERMUTATION */
            if(permutation_iterator != (permutation.end() - 1)) {
                ++permutation_iterator;
            } else {
                permutate();
            }
        }
        current_sound->play();
    }
    ++countBeats;
}

void SoundThread::stop() {
    if(countBeats > beatsPerNote)
        current_sound->stop();
}

void SoundThread::bpmChanged(int value) {
    std::cout << "BPM changed to: " << value << std::endl;
    interval = 60000/value;
    timer->setInterval(interval);
}

void SoundThread::volumeChangedMaster(int value) {                          // NOT WORKING PROPERLY
    metronome->setVolume(metronome->volume() * ((float)value/100.0f));

    for(auto e: sound_map.keys()) {
        sound_map[e]->setVolume(sound_map[e]->volume() * ((float)value/100.0f));
    }
}

void SoundThread::volumeChangedMetronome(int value) {
    metronome->setVolume((float)value/100.0f);
    metronome_emph->setVolume((float)value/100.0f);
}

void SoundThread::volumeChangedSound(int value) {
    for(auto e: sound_map.keys()) {
        sound_map[e]->setVolume((float)value/100.0f);
    }
}

void SoundThread::permutate() {
    if(permutation.size() != 0)
        permutation.clear();

    std::srand(unsigned(std::time(0)));
    for(int i = 0; i < (int)checked_notes->size(); ++i)
        permutation.push_back(i);

    std::cout << "Permutation size: " << permutation.size() << std::endl;

    std::random_shuffle(permutation.begin(), permutation.end());

    //Print out Permutation
    std::cout << "Permutation: ";
    for(std::vector<int>::iterator it = permutation.begin(); it != permutation.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;

    permutation_iterator = permutation.begin();
}

void SoundThread::setMetronomeSound(const QString& name, bool emphasized) {
    if(name == "metronome-clave" ||
            name == "metronome-hihat-closed" ||
            name == "metronome-rim") {
        if(emphasized) {
            metronome_emph->setSource(metronome_map[name]);
        } else {
            metronome->setSource(metronome_map[name]);
        }
    } else {
        std::cout << "Error: Metronome soundname not found: " << name.toStdString() << std::endl;
    }
}

void SoundThread::loadFiles() {
    /* Load Metronome Sounds */
    metronome_map.insert("metronome-clave", QUrl::fromLocalFile(QFileInfo("../sounds/sound-clave-metronome.wav").absoluteFilePath()));
    metronome_map.insert("metronome-hihat-closed", QUrl::fromLocalFile(QFileInfo("../sounds/sound-hihat-closed-metronome.wav").absoluteFilePath()));
    metronome_map.insert("metronome-rim", QUrl::fromLocalFile(QFileInfo("../sounds/sound-rim-metronome.wav").absoluteFilePath()));

    metronome = new QSoundEffect;
    metronome_emph = new QSoundEffect;

    /* Load Sounds */
    QSoundEffect* temp;
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-a.wav").absoluteFilePath()));
    sound_map.insert(A, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-as.wav").absoluteFilePath()));
    sound_map.insert(AS, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-be.wav").absoluteFilePath()));
    sound_map.insert(BE, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-c.wav").absoluteFilePath()));
    sound_map.insert(C, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-cis.wav").absoluteFilePath()));
    sound_map.insert(CIS, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-d.wav").absoluteFilePath()));
    sound_map.insert(D, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-e.wav").absoluteFilePath()));
    sound_map.insert(E, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-es.wav").absoluteFilePath()));
    sound_map.insert(ES, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-f.wav").absoluteFilePath()));
    sound_map.insert(F, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-fis.wav").absoluteFilePath()));
    sound_map.insert(FIS, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-g.wav").absoluteFilePath()));
    sound_map.insert(G, temp);
    temp = new QSoundEffect();
    temp->setSource(QUrl::fromLocalFile(QFileInfo("../sounds/note-h.wav").absoluteFilePath()));
    sound_map.insert(H, temp);
}
